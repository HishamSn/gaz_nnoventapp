package com.noventapp.task.hisham.task_noventapp.utils;

import android.content.Context;

import com.noventapp.task.hisham.task_noventapp.R;

/**
 * Created by Hisham Snaimeh on 4/17/2018.
 * hish.sn.dev@gmail.com
 */

public class ResourcesUtil {

    public static int getDrawableByName(String name,Context context) {
        return context.getResources().getIdentifier(
                name,
                context.getString(R.string.drawable),
                context.getPackageName()
        );
    }
}
