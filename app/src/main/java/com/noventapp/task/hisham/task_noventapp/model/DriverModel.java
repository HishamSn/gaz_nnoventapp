package com.noventapp.task.hisham.task_noventapp.model;

/**
 * Created by Hisham Snaimeh on 4/17/2018.
 * hish.sn.dev@gmail.com
 */

public class DriverModel {

    private String iconStatus;
    private String name;
    private String number;
    private String time;
    private String response;

    public String getIconStatus() {
        return iconStatus;
    }

    public void setIconStatus(String iconStatus) {
        this.iconStatus = iconStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
