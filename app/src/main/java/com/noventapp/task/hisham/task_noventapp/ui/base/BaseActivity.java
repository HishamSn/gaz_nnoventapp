package com.noventapp.task.hisham.task_noventapp.ui.base;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.noventapp.task.hisham.task_noventapp.utils.LocalHelper;

import static com.noventapp.task.hisham.task_noventapp.constant.AppConstant.AR;

/**
 * Created by Hisham Snaimeh on 4/16/2018.
 * hish.sn.dev@gmail.com
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context base) {
        LocalHelper.setLocale(base, AR);
        super.attachBaseContext(LocalHelper.onAttach(base));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
