package com.noventapp.task.hisham.task_noventapp.utils;


import android.content.Context;
import android.util.Log;
import java.io.*;

import static com.noventapp.task.hisham.task_noventapp.constant.AppConstant.*;

public class ReaderUtil {


    public static String getResponse(int jsonRaw,Context context) {
        InputStream resourceReader = context.
                getResources().openRawResource(jsonRaw);
        Writer writer = new StringWriter();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceReader, "UTF-8"));
            String line = reader.readLine();
            while (line != null) {
                writer.write(line);
                line = reader.readLine();
            }
        } catch (Exception e) {
            Log.e(TAG, "Unhandled exception while using JSONResourceReader", e);
        } finally {
            try {
                resourceReader.close();
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception while using JSONResourceReader", e);
            }
        }
        return writer.toString();
    }
}
