package com.noventapp.task.hisham.task_noventapp.ui.user;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.*;
import android.view.*;
import android.widget.*;

import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.model.InfoModel;
import com.noventapp.task.hisham.task_noventapp.utils.ResourcesUtil;

import java.util.*;


public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.ViewHolder> {

    private List<InfoModel> infoModelList = new ArrayList<>();
    private Context context;
    private int heightScreenRV;

    public InfoAdapter(List<InfoModel> infoModelList) {
        this.infoModelList = infoModelList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context =  recyclerView.getContext();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_info_user, parent, false);
        heightScreenRV = parent.getMeasuredHeight();

        view.setLayoutParams(new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                heightScreenRV / 3
        ));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InfoAdapter.ViewHolder holder, int position) {

        if ((position + 1) % 2 == 0) {
            holder.separatorView.setVisibility(View.GONE);
        }

        holder.labelTV.setText(infoModelList.get(position).getLabel());
        holder.contentTV.setText(infoModelList.get(position).getContent());
        holder.iconImageView.setImageResource(ResourcesUtil.getDrawableByName(
                infoModelList.get(position).getIcon(), context));
    }

    @Override
    public int getItemCount() {
        return infoModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout constraintLayout;
        AppCompatImageView iconImageView;
        TextView labelTV;
        TextView contentTV;
        View separatorView;

        ViewHolder(View v) {
            super(v);
            constraintLayout = v.findViewById(R.id.cl_info_user);
            iconImageView = v.findViewById(R.id.iv_icon);
            labelTV = v.findViewById(R.id.tv_label);
            contentTV = v.findViewById(R.id.tv_content);
            separatorView = v.findViewById(R.id.view_separator);
        }
    }
}
