package com.noventapp.task.hisham.task_noventapp.model;

/**
 * Created by Hisham Snaimeh on 4/17/2018.
 * hish.sn.dev@gmail.com
 */

public class InfoModel {
    private String icon;
    private String label;
    private String content;


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
