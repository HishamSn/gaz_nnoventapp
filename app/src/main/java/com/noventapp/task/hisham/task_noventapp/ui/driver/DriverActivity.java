package com.noventapp.task.hisham.task_noventapp.ui.driver;

import android.os.Bundle;
import androidx.appcompat.widget.*;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.model.*;
import com.noventapp.task.hisham.task_noventapp.ui.base.BaseActivity;
import com.noventapp.task.hisham.task_noventapp.utils.*;

import java.util.List;

public class DriverActivity extends BaseActivity {
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        init();
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(
                new LinearLayoutManager(this));


        recyclerView.setAdapter(new DriverAdapter(getDriverList()));
    }

    private void init() {
        recyclerView = findViewById(R.id.rv_order_status);
    }

    private List<DriverModel> getDriverList() {
        return GsonUtil.getGson().fromJson(ReaderUtil.getResponse(R.raw.driver_users, this)
                , new TypeToken<List<DriverModel>>() {
                }.getType());
    }

}
