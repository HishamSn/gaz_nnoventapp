package com.noventapp.task.hisham.task_noventapp.ui.main;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.*;
import android.view.*;

import androidx.recyclerview.widget.RecyclerView;

import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.ui.driver.DriverActivity;
import com.noventapp.task.hisham.task_noventapp.ui.user.*;

import java.util.*;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<Integer> mainList = new ArrayList<>();
    private AppCompatActivity parentActivity;

    public MainAdapter(List<Integer> mainList) {
        this.mainList = mainList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentActivity = (AppCompatActivity) recyclerView.getContext();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false);

        view.setLayoutParams(new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                parent.getMeasuredHeight() / 3
        ));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainAdapter.ViewHolder holder, int position) {

        holder.bgImageView.setImageResource(mainList.get(position));

        holder.itemView.setOnClickListener(v -> {
            switch (position) {
                case 0:
                    goToActivity(DriverActivity.class);
                    break;
                case 1:
                    goToActivity(UserInfoActivity.class);
                    break;
                case 2:
                    goToActivity(UserLocationActivity.class);
                    break;
            }
        });
    }

    private void goToActivity(Class activity) {
        parentActivity.startActivity(new Intent
                (parentActivity, activity));
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView bgImageView;

        ViewHolder(View v) {
            super(v);
            bgImageView = v.findViewById(R.id.iv_main);
        }
    }
}
