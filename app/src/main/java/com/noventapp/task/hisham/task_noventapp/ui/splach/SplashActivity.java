package com.noventapp.task.hisham.task_noventapp.ui.splach;

import android.content.Intent;
import android.os.*;

import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.ui.base.BaseActivity;
import com.noventapp.task.hisham.task_noventapp.ui.main.MainActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setHandler(1);
    }

    private void setHandler(int secondsDelayed) {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }, secondsDelayed * 1000);
    }
}

