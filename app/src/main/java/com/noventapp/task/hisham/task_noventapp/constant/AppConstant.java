package com.noventapp.task.hisham.task_noventapp.constant;

/**
 * Created by Hisham Snaimeh on 4/17/2018.
 * hish.sn.dev@gmail.com
 */

public class AppConstant {
    public static final String TAG = "NOVENTAPP";
    public static final String AR = "ar";
}
