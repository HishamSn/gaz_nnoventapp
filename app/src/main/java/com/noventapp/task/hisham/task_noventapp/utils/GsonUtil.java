package com.noventapp.task.hisham.task_noventapp.utils;

import com.google.gson.*;

public class GsonUtil {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static Gson gson = new GsonBuilder()
            .setDateFormat(DATE_FORMAT)
            .create();

    private GsonUtil(){
    }

    public static Gson getGson () {
        return gson;
    }
}
