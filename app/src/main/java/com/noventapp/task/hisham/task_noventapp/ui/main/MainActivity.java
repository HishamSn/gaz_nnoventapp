package com.noventapp.task.hisham.task_noventapp.ui.main;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.*;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.ui.base.BaseActivity;

import java.util.*;

public class MainActivity extends BaseActivity {
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setUpRecyclerView(getMainList());
    }

    private void init() {
        recyclerView = findViewById(R.id.rv_main);
    }

    private void setUpRecyclerView(List<Integer> mainList) {
        recyclerView.setLayoutManager(
                new LinearLayoutManager(this));

        recyclerView.setAdapter(new MainAdapter(mainList));
    }

    @NonNull
    private List<Integer> getMainList() {
        List<Integer> mainList = new ArrayList<>();
        mainList.add(R.drawable.bg_screen_1);
        mainList.add(R.drawable.bg_screen_2);
        mainList.add(R.drawable.bg_screen_3);

        return mainList;
    }

}
