package com.noventapp.task.hisham.task_noventapp.ui.user;

import android.os.Bundle;
import androidx.appcompat.widget.*;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.model.InfoModel;
import com.noventapp.task.hisham.task_noventapp.ui.base.BaseActivity;
import com.noventapp.task.hisham.task_noventapp.utils.*;

import java.util.*;

public class UserInfoActivity extends BaseActivity {
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        init();
        setUpRecyclerView();
    }

    private void init() {
        recyclerView = findViewById(R.id.rv_info);
    }

    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(
                new GridLayoutManager(this, 2));
        recyclerView.setAdapter(new InfoAdapter(getInfoList()));
    }

    private List<InfoModel> getInfoList() {
        return GsonUtil.getGson().fromJson(ReaderUtil.getResponse(R.raw.user_information, this)
                , new TypeToken<List<InfoModel>>() {
                }.getType());
    }

}
