package com.noventapp.task.hisham.task_noventapp.ui.user;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.ui.base.BaseActivity;

public class UserLocationActivity extends BaseActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_location);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.setTrafficEnabled(false);

        LatLng sydney = new LatLng(31.987528, 35.903180);
        mMap.addMarker(new MarkerOptions().position(sydney)
                .title("Hisham Snaimeh"));
        mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                        sydney, 17
                )
        );
    }
}
