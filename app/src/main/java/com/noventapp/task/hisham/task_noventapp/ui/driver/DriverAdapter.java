package com.noventapp.task.hisham.task_noventapp.ui.driver;

import android.content.Context;
import androidx.appcompat.widget.*;
import android.view.*;

import androidx.recyclerview.widget.RecyclerView;

import com.noventapp.task.hisham.task_noventapp.R;
import com.noventapp.task.hisham.task_noventapp.model.*;
import com.noventapp.task.hisham.task_noventapp.utils.ResourcesUtil;

import java.util.*;


public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.ViewHolder> {
    private Context context;
    private List<DriverModel> driverModelList = new ArrayList<>();


    public DriverAdapter(List<DriverModel> driverModelList) {
        this.driverModelList = driverModelList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        context = recyclerView.getContext();
        super.onAttachedToRecyclerView(recyclerView);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_order_status;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.ivStatus.setImageResource(ResourcesUtil.getDrawableByName(
                driverModelList.get(position).getIconStatus(), context));
        holder.tvName.setText(driverModelList.get(position).getName());
        holder.tvNumber.setText(driverModelList.get(position).getNumber());
        holder.tvTime.setText(driverModelList.get(position).getTime());
        holder.tvResponse.setText(driverModelList.get(position).getResponse());

        holder.ivMenu.setOnClickListener(this::showPopup);
    }


    @Override
    public int getItemCount() {
        return driverModelList.size();
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu, popup.getMenu());
        popup.show();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView ivMenu;
        AppCompatImageView ivStatus;
        AppCompatTextView tvName;
        AppCompatTextView tvNumber;
        AppCompatTextView tvTime;
        AppCompatTextView tvResponse;


        ViewHolder(View v) {
            super(v);
            ivMenu = v.findViewById(R.id.iv_menu);
            ivStatus = v.findViewById(R.id.iv_status);
            tvName = v.findViewById(R.id.tv_name);
            tvNumber = v.findViewById(R.id.tv_number);
            tvTime = v.findViewById(R.id.tv_time_content);
            tvResponse = v.findViewById(R.id.tv_response_content);
        }

    }
}
